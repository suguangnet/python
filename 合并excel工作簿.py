import xlrd  #pip install xlrd==1.2.0 1.2.0版本不支持xlsx
import xlsxwriter
import glob
biao_tou = []
# 获取要合并的所有exce表格


def get_exce():
    all_exce = glob.glob("*.xls*")
    print("该目录下有" + str(len(all_exce)) + "个exce文件：")
    if (len(all_exce) == 0):
        return 0
    else:
        for i in range(len(all_exce)):
            print(all_exce[i])
        return all_exce
# 打开Exce文件


def open_exce(name):
    fh = xlrd.open_workbook(name)
    return fh
# 获取exce文件下的所有sheet


def get_sheet(fh):
    sheets = fh.sheets()
    return sheets
# 获取sheet下有多少行数据


def get_sheetrow_num(sheet):
    return sheet.nrows
# 获取sheet下的数据


def get_sheet_data(sheet, row, biao_tou_num):
    for i in range(row):
        if (i < biao_tou_num):
            global biao_tou
            values = sheet.row_values(i)
            biao_tou.append(values)
            continue
        values = sheet.row_values(i)
        all_data1.append(values)
    return all_data1
# 获取表头数量


def get_biao_tou_num(exce1, exce2):
    fh = open_exce(exce1)
    fhx = open_exce(exce2)
    sheet_1 = fh.sheet_by_index(0)
    sheet_2 = fhx.sheet_by_index(0)
    row_sum_1 = sheet_1.nrows
    row_sum_2 = sheet_2.nrows
    # 获取第一张sheet表对象有效行数
    # 获取sheet表某一行所有数据类型及值
    for i in range(row_sum_1):
        # 获取sheet表对象某一行数据值
        if (i+1 == row_sum_2):
            return i
        #row_0_value = sheet_1.row_values(0)
        row_content_1 = sheet_1.row_values(i)
        row_content_2 = sheet_2.row_values(i)
        if (row_content_1 == row_content_2):
            continue
        else:
            return i

        # 防止越界
if __name__ == '__main__':
    print("合并Excel工作薄，把程序放到同目录")
    print("如有问题联系：速光网络")
    print("https://gitee.com/suguangnet/python\n\n\n")
    all_exce = get_exce()
    # 得到要合并的所有exce表格数据
    if (all_exce == 0):
        print("该目录下无.xlsx文件！请把程序移动到要合并的表格同目录下！")
        end = input("按Enter结束程序")
        exit()
    if (len(all_exce) == 1):
        print("该目录下只有一个.xlsx文件！无需合并")
        end = input("按Enter结束程序")
        exit()
    # 表头数
    print("自动检测表头中......")
    biao_tou_num = get_biao_tou_num(all_exce[0], all_exce[1])
    print("表头数为:", biao_tou_num,)
    guess = input("y/n?")
    if (guess == "n"):
        biao_tou_num = input("请输入表头数:")
        biao_tou_num = int(biao_tou_num)
    all_data1 = []
    # 用于保存合并的所有行的数据
    # 下面开始文件数据的获取
    for exce in all_exce:
        fh = open_exce(exce)
        # 打开文件
        sheets = get_sheet(fh)
        # 获取文件下的sheet数量
        for sheet in range(len(sheets)):
            row = get_sheetrow_num(sheets[sheet])
            # 获取一个sheet下的所有的数据的行数
            all_data2 = get_sheet_data(sheets[sheet], row, biao_tou_num)
            # 获取一个sheet下的所有行的数据
    for i in range(biao_tou_num):
        all_data2.insert(i, biao_tou[i])
    # 表头写入
    new_name = input("请输入新表的名称:")
    # 下面开始文件数据的写入
    new_exce = new_name+".xlsx"
    # 新建的exce文件名字
    fh1 = xlsxwriter.Workbook(new_exce)
    # 新建一个exce表
    new_sheet = fh1.add_worksheet()
    # 新建一个sheet表
    for i in range(len(all_data2)):
        for j in range(len(all_data2[i])):
            c = all_data2[i][j]
            new_sheet.write(i, j, c)
    fh1.close()
    # 关闭该exce表
    print("文件合并成功,请查看"+new_exce+"文件！")
    end = input("按Enter结束程序")
