import xlwings as xw
app = xw.App(visible=False, add_book=False)
ex = app.books.open("1.xlsx")
ex_bak = app.books.open("2.xlsx")
for row in ex.sheets[0].range("A1").expand():
    for cell in row:
        bak_cell = ex_bak.sheets[0].range(cell.address)
    if cell.value != bak_cell.value:
        cell.color = bak_cell.color = (255, 0, 0)
        print(cell.color)
ex.save()
ex.close()
ex_bak.save()
ex_bak.close()
app.quit()
