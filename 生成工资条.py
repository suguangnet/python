from venv import create
from openpyxl import load_workbook,Workbook
def create_excel():
    wb = load_workbook('工资.xlsx',data_only=True)
    sh=wb.active
    title=['工号','姓名','部门','基本工资','提成','加班工资','社保扣除','考勤扣除','应发工资','备注']
    for i,row in enumerate(sh.rows):
        if i==0:
            continue
        else:
            temp_wbook=Workbook()
            temp_sh=temp_wbook.active
            row_data=[cell.value for cell in row]
            temp_sh.append(row_data)
            print(row_data)
            temp_wbook.save(f'./{row_data[1]}.xlsx')
if __name__=="__main__":
    create_excel()