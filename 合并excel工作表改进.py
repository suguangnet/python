import xlrd
import xlwt
import pandas as pd #导入pandas模块
xlsx = ("2022.xlsx")
df = pd.read_excel(xlsx, sheet_name=None) #导入需要合并的工作表，sheet_name=None时是返回工作簿中的全部工作表，如果需要指定工作表时可更改为工作表名称。
workBook = xlrd.open_workbook(xlsx) # 获取所有的sheet的名字
allSheetNames = workBook.sheet_names()
print('合并的Sheet有：')
print(allSheetNames)
df = pd.concat(df) #用concat函数进行合并
df.to_excel('./汇总.xlsx', index=False) #保存，index=False取消索引